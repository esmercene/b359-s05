from abc import ABC, abstractmethod

class Person(ABC):

    def __init__(self, first_name, last_name, email, department):
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department
    
    def getFullName(self):
        return f"{self._first_name} {self._last_name}"
    
    @abstractmethod
    def addRequest(self):
        pass
    
    @abstractmethod
    def checkRequest(self):
        pass
    
    @abstractmethod
    def addUser(self):
        pass


class Employee(Person):
    
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name, email, department)
        
    def login(self):
        return f"{self._email} has logged in"
    
    def logout(self):
        return f"{self._email} has logged out"
    
    def checkRequest(self):
        pass
    
    def addUser(self):
        pass
    
    def addRequest(self):
        return "Request has been added"


class TeamLead(Person):
    
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name, email, department)
        self._members = []
        
    def addMember(self, employee):
        self._members.append(employee)
        return "Member has been added"
    
    def getMembers(self):
        return self._members
    
    def login(self):
        return f"{self._email} has logged in"
    
    def logout(self):
        return f"{self._email} has logged out"
    
    def addRequest(self):
        return "Request has been added"
    
    def addUser(self):
        pass
    
    def checkRequest(self):
        pass


class Admin(Person):
    
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name, email, department)
        
    def login(self):
        return f"{self._email} has logged in"
    
    def logout(self):
        return f"{self._email} has logged out"
    
    def addUser(self):
        return "User has been added"
    
    def checkRequest(self):
        pass
    
    def addRequest(self):
        return "Request has been added"


class Request():
    
    def __init__(self, name, requester, date_requested, status="open"):
        self._name = name
        self._requester = requester
        self._date_requested = date_requested
        self._status = status
    
    def closeRequest(self):
        self._status = "closed"
        return f"Request {self._name} has been closed"






# Test cases
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

# Adding Members and printing their full names
teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.getMembers():
    print(indiv_emp.getFullName())


# Additional Assertions
assert admin1.addUser() == "User has been added"

# Closing request and printing the message
print(req2.closeRequest())

















